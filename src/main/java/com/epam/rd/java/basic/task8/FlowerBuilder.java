package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.entity.Flower;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class FlowerBuilder {

    private FlowerBuilder(){};

    public static Flower getFlower(Node flowerNode){
        Flower flower = new Flower();

        flower.name        = getValue("name", flowerNode);
        flower.soil        = getValue("soil", flowerNode);
        flower.origin      = getValue("origin", flowerNode);
        flower.multiplying = getValue("multiplying", flowerNode);

        flower.visualParameters.stemColour = getValue("stemColour", flowerNode);
        flower.visualParameters.leafColour = getValue("leafColour", flowerNode);

        flower.visualParameters.aveLenFlower.content = getValue("aveLenFlower", flowerNode);
        flower.visualParameters.aveLenFlower.measure = getAttr("aveLenFlower", "measure", flowerNode);


        flower.growingTips.tempreture.content = getValue("tempreture", flowerNode);
        flower.growingTips.tempreture.measure = getAttr("tempreture", "measure", flowerNode);

        flower.growingTips.watering.content = getValue("watering", flowerNode);
        flower.growingTips.watering.measure = getAttr("watering", "measure", flowerNode);

        flower.growingTips.lighting = getAttr("lighting", "lightRequiring", flowerNode);

        return flower;
    }

    public static String getValue(String name, Node node){
        Element element = (Element) node;

        if (element.getNodeType() == Node.ELEMENT_NODE){
            return element.getElementsByTagName(name).item(0).getTextContent();
        }
        return null;
    }

    public static String getAttr(String elName, String attrName,  Node flowerNode){
        Element element = (Element) flowerNode;

        if (element.getNodeType() == Node.ELEMENT_NODE){
            return ((Element) element.getElementsByTagName(elName).item(0)).getAttribute(attrName);
        }
        return null;
    }
}
