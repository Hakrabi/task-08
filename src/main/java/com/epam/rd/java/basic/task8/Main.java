package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;

import java.util.Comparator;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		DOMController domController = new DOMController(xmlFileName);
		List<Flower> flowerListDom = domController.parse();

		flowerListDom.sort((Comparator.comparing(o -> o.name)));

		String outputXmlFile = "output.dom.xml";
		domController.write(flowerListDom, outputXmlFile);


		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		SAXController saxController = new SAXController(xmlFileName);
		List<Flower> flowerListSAX = saxController.parse();

		flowerListSAX.sort((Comparator.comparing(o -> o.name)));
		System.out.println(flowerListSAX);

		outputXmlFile = "output.sax.xml";
		saxController.write(flowerListSAX, outputXmlFile);


		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		STAXController staxController = new STAXController(xmlFileName);
		List<Flower> flowerListSTAX = staxController.parse();

		flowerListSTAX.sort((Comparator.comparing(o -> o.name)));
		System.out.println(flowerListSTAX);

		outputXmlFile = "output.stax.xml";
		staxController.write(flowerListSTAX, outputXmlFile);
	}

}
