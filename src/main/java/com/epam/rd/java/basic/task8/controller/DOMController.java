package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.FlowerBuilder;
import com.epam.rd.java.basic.task8.entity.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> parse() throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document doc = builder.parse(new File(xmlFileName));

		NodeList nodeList = doc.getElementsByTagName("flower");

		List<Flower> flowerList = new ArrayList<>();
		for (int i = 0; i < nodeList.getLength(); i++) {

			Node node = nodeList.item(i);
			flowerList.add(FlowerBuilder.getFlower(node));

		}


		return flowerList;
	}

	public void write(List<Flower> flowerList, String outputXmlFile) throws ParserConfigurationException {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document doc = builder.newDocument();

		Element root = doc.createElement("flowers");

		root.setAttribute("xmlns", "http://www.nure.ua");
		root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");

		Element e = null;

		for (Flower flower : flowerList) {
			Element flowerElement = doc.createElement("flower");
			Element visualParametersElement = doc.createElement("visualParameters");
			Element growingTipsElement = doc.createElement("growingTips");

			root.appendChild(flowerElement);

			e = doc.createElement("name");
			e.appendChild(doc.createTextNode(flower.name));
			flowerElement.appendChild(e);

			e = doc.createElement("soil");
			e.appendChild(doc.createTextNode(flower.soil));
			flowerElement.appendChild(e);

			e = doc.createElement("origin");
			e.appendChild(doc.createTextNode(flower.origin));
			flowerElement.appendChild(e);


			e = doc.createElement("stemColour");
			e.appendChild(doc.createTextNode(flower.visualParameters.stemColour));
			visualParametersElement.appendChild(e);

			e = doc.createElement("leafColour");
			e.appendChild(doc.createTextNode(flower.visualParameters.leafColour));
			visualParametersElement.appendChild(e);

			e = doc.createElement("aveLenFlower");
			e.appendChild(doc.createTextNode(flower.visualParameters.aveLenFlower.content));
			e.setAttribute("measure", flower.visualParameters.aveLenFlower.measure);
			visualParametersElement.appendChild(e);

			flowerElement.appendChild(visualParametersElement);


			e = doc.createElement("tempreture");
			e.appendChild(doc.createTextNode(flower.growingTips.tempreture.content));
			e.setAttribute("measure", flower.growingTips.tempreture.measure);

			growingTipsElement.appendChild(e);

			e = doc.createElement("lighting");
			e.setAttribute("lightRequiring", flower.growingTips.lighting);
			growingTipsElement.appendChild(e);

			e = doc.createElement("watering");
			e.appendChild(doc.createTextNode(flower.growingTips.watering.content));
			e.setAttribute("measure", flower.growingTips.watering.measure);
			growingTipsElement.appendChild(e);

			flowerElement.appendChild(growingTipsElement);

			e = doc.createElement("multiplying");
			e.appendChild(doc.createTextNode(flower.multiplying));
			flowerElement.appendChild(e);

			root.appendChild(flowerElement);

		}

		doc.appendChild(root);

		try {
			Transformer tr = TransformerFactory.newInstance().newTransformer();
			tr.setOutputProperty(OutputKeys.INDENT, "yes");
			tr.setOutputProperty(OutputKeys.METHOD, "xml");
			tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");


			tr.transform(new DOMSource(doc), new StreamResult(new FileOutputStream(outputXmlFile)));

		} catch (FileNotFoundException | TransformerException transformerConfigurationException) {
			transformerConfigurationException.printStackTrace();
		}


	}

}
//	xml version="1.0" encoding="UTF-8"?>
//
//<flowers xmlns="http://www.nure.ua"
//		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
//		xsi:schemaLocation="http://www.nure.ua input.xsd ">