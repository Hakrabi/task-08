package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	List<Flower> flowerList;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}


	public List<Flower> parse() throws ParserConfigurationException, SAXException, IOException {
		SAXParser parser = SAXParserFactory.newInstance().newSAXParser();

		flowerList = new ArrayList<>();
		parser.parse(xmlFileName, new XMLHandler());

		return flowerList;
	}

	public void write(List<Flower> flowerList, String outputXmlFile) throws FileNotFoundException, UnsupportedEncodingException, XMLStreamException {
		OutputStream outputStream = new FileOutputStream(outputXmlFile);

		XMLStreamWriter out = XMLOutputFactory.newInstance().createXMLStreamWriter(
				new OutputStreamWriter(outputStream, "utf-8"));


		out.writeStartDocument();

		out.writeStartElement("flowers");
		out.writeAttribute("xmlns", "http://www.nure.ua" );
		out.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance" );
		out.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd " );

		for(Flower flower : flowerList){
			out.writeStartElement("flower");

				out.writeStartElement("name");
				out.writeCharacters(flower.name);
				out.writeEndElement();

				out.writeStartElement("soil");
				out.writeCharacters(flower.soil);
				out.writeEndElement();

				out.writeStartElement("origin");
				out.writeCharacters(flower.origin);
				out.writeEndElement();

				out.writeStartElement("visualParameters");
					out.writeStartElement("stemColour");
					out.writeCharacters(flower.visualParameters.stemColour);
					out.writeEndElement();

					out.writeStartElement("leafColour");
					out.writeCharacters(flower.visualParameters.leafColour);
					out.writeEndElement();

					out.writeStartElement("aveLenFlower");
					out.writeAttribute("measure", flower.visualParameters.aveLenFlower.measure);
					out.writeCharacters(flower.visualParameters.aveLenFlower.content);
					out.writeEndElement();
				out.writeEndElement();

				out.writeStartElement("growingTips");
					out.writeStartElement("tempreture");
					out.writeAttribute("measure", flower.growingTips.tempreture.measure);
					out.writeCharacters(flower.growingTips.tempreture.content);
					out.writeEndElement();

					out.writeEmptyElement("lighting");
					out.writeAttribute("lightRequiring", flower.growingTips.lighting);

					out.writeStartElement("watering");
					out.writeAttribute("measure", flower.growingTips.watering.measure);
					out.writeCharacters(flower.growingTips.watering.content);
					out.writeEndElement();
				out.writeEndElement();

				out.writeStartElement("multiplying");
				out.writeCharacters(flower.multiplying);
				out.writeEndElement();

			out.writeEndElement();
		}

		out.writeEndElement();
		out.writeEndDocument();

		out.close();
	}

	private class XMLHandler extends DefaultHandler{
		private String lastElementName;
		private Flower flower = new Flower();

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			if(qName.equals("aveLenFlower"))
				flower.visualParameters.aveLenFlower.measure = attributes.getValue("measure");
			if(qName.equals("lighting"))
				flower.growingTips.lighting = attributes.getValue("lightRequiring");
			if(qName.equals("watering"))
				flower.growingTips.watering.measure = attributes.getValue("measure");
			if(qName.equals("tempreture"))
				flower.growingTips.tempreture.measure = attributes.getValue("measure");

			lastElementName = qName;
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			String information = new String(ch, start, length);

			information = information.replace("\n", "").trim();

			if (!information.isEmpty()) {
				if (lastElementName.equals("name"))
					flower.name = information;
				if (lastElementName.equals("soil"))
					flower.soil = information;
				if (lastElementName.equals("origin"))
					flower.origin = information;
				if (lastElementName.equals("multiplying"))
					flower.multiplying = information;

				if (lastElementName.equals("leafColour"))
					flower.visualParameters.leafColour = information;
				if (lastElementName.equals("aveLenFlower"))
					flower.visualParameters.aveLenFlower.content = information;
				if (lastElementName.equals("stemColour"))
					flower.visualParameters.stemColour = information;

				if (lastElementName.equals("watering"))
					flower.growingTips.watering.content = information;
				if (lastElementName.equals("tempreture"))
					flower.growingTips.tempreture.content = information;
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if ( flower.isFilled() ) {
				flowerList.add(flower);
				flower = new Flower();
			}
		}
	}

}