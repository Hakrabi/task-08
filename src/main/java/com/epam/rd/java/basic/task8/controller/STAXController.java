package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> parse() throws FileNotFoundException, XMLStreamException {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));

		Flower flower = null;
		List<Flower> flowerList = new ArrayList<>();

		while (reader.hasNext()){
			XMLEvent xmlEvent = reader.nextEvent();

			if (xmlEvent.isStartElement()) {
				StartElement startElement = xmlEvent.asStartElement();

				if (startElement.getName().getLocalPart().equals("flower")) {
					flower = new Flower();

				} else if (startElement.getName().getLocalPart().equals("name")) {
					xmlEvent = reader.nextEvent();
					flower.name = xmlEvent.asCharacters().getData();
				} else if (startElement.getName().getLocalPart().equals("soil")) {
					xmlEvent = reader.nextEvent();
					flower.soil = xmlEvent.asCharacters().getData();
				} else if (startElement.getName().getLocalPart().equals("origin")) {
					xmlEvent = reader.nextEvent();
					flower.origin = xmlEvent.asCharacters().getData();
				}else if (startElement.getName().getLocalPart().equals("multiplying")) {
					xmlEvent = reader.nextEvent();
					flower.multiplying = xmlEvent.asCharacters().getData();
				}else if (startElement.getName().getLocalPart().equals("stemColour")) {
					xmlEvent = reader.nextEvent();
					flower.visualParameters.stemColour = xmlEvent.asCharacters().getData();
				}else if (startElement.getName().getLocalPart().equals("leafColour")) {
					xmlEvent = reader.nextEvent();
					flower.visualParameters.leafColour = xmlEvent.asCharacters().getData();
				}else if (startElement.getName().getLocalPart().equals("aveLenFlower")) {
					xmlEvent = reader.nextEvent();
					flower.visualParameters.aveLenFlower.measure = startElement.getAttributeByName(new QName("measure")).getValue();
					flower.visualParameters.aveLenFlower.content = xmlEvent.asCharacters().getData();
				}else if (startElement.getName().getLocalPart().equals("lighting")) {
					flower.growingTips.lighting = startElement.getAttributeByName(new QName("lightRequiring")).getValue();
				}else if (startElement.getName().getLocalPart().equals("tempreture")) {
					xmlEvent = reader.nextEvent();
					flower.growingTips.tempreture.measure = startElement.getAttributeByName(new QName("measure")).getValue();
					flower.growingTips.tempreture.content = xmlEvent.asCharacters().getData();
				}else if (startElement.getName().getLocalPart().equals("watering")) {
					xmlEvent = reader.nextEvent();
					flower.growingTips.watering.measure = startElement.getAttributeByName(new QName("measure")).getValue();
					flower.growingTips.watering.content = xmlEvent.asCharacters().getData();
				}

			}
			// если цикл дошел до закрывающего элемента Student,
			// то добавляем считанного из файла студента в список
			if (xmlEvent.isEndElement()) {
				EndElement endElement = xmlEvent.asEndElement();
				if (endElement.getName().getLocalPart().equals("flower")) {
					flowerList.add(flower);
				}
			}
		}
		return flowerList;
	}

	public void write(List<Flower> flowerList, String outputXmlFile) throws FileNotFoundException, UnsupportedEncodingException, XMLStreamException {
		OutputStream outputStream = new FileOutputStream(outputXmlFile);

		XMLStreamWriter out = XMLOutputFactory.newInstance().createXMLStreamWriter(
				new OutputStreamWriter(outputStream, "utf-8"));


		out.writeStartDocument();

		out.writeStartElement("flowers");
		out.writeAttribute("xmlns", "http://www.nure.ua" );
		out.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance" );
		out.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd " );

		for(Flower flower : flowerList){
			out.writeStartElement("flower");

			out.writeStartElement("name");
			out.writeCharacters(flower.name);
			out.writeEndElement();

			out.writeStartElement("soil");
			out.writeCharacters(flower.soil);
			out.writeEndElement();

			out.writeStartElement("origin");
			out.writeCharacters(flower.origin);
			out.writeEndElement();

			out.writeStartElement("visualParameters");
			out.writeStartElement("stemColour");
			out.writeCharacters(flower.visualParameters.stemColour);
			out.writeEndElement();

			out.writeStartElement("leafColour");
			out.writeCharacters(flower.visualParameters.leafColour);
			out.writeEndElement();

			out.writeStartElement("aveLenFlower");
			out.writeAttribute("measure", flower.visualParameters.aveLenFlower.measure);
			out.writeCharacters(flower.visualParameters.aveLenFlower.content);
			out.writeEndElement();
			out.writeEndElement();

			out.writeStartElement("growingTips");
			out.writeStartElement("tempreture");
			out.writeAttribute("measure", flower.growingTips.tempreture.measure);
			out.writeCharacters(flower.growingTips.tempreture.content);
			out.writeEndElement();

			out.writeEmptyElement("lighting");
			out.writeAttribute("lightRequiring", flower.growingTips.lighting);

			out.writeStartElement("watering");
			out.writeAttribute("measure", flower.growingTips.watering.measure);
			out.writeCharacters(flower.growingTips.watering.content);
			out.writeEndElement();
			out.writeEndElement();

			out.writeStartElement("multiplying");
			out.writeCharacters(flower.multiplying);
			out.writeEndElement();

			out.writeEndElement();
		}

		out.writeEndElement();
		out.writeEndDocument();

		out.close();
	}
}