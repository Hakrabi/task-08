package com.epam.rd.java.basic.task8.entity;


public class Flower {

    public String name;
    public String soil;
    public String origin;
    public String multiplying;

    public VisualParameters visualParameters;
    public GrowingTips growingTips;

    public Flower(){
        visualParameters = new VisualParameters();
        growingTips = new GrowingTips();
    }

    public boolean isFilled(){
        return  (name != null && !name.isEmpty()) &&
                (soil != null && !soil.isEmpty()) &&
                (origin != null && !origin.isEmpty()) &&
                (multiplying != null && !multiplying.isEmpty()) &&

                (visualParameters.stemColour != null && !visualParameters.stemColour.isEmpty()) &&
                (visualParameters.leafColour != null && !visualParameters.leafColour.isEmpty()) &&
                (visualParameters.aveLenFlower.measure != null && !visualParameters.aveLenFlower.measure.isEmpty()) &&
                (visualParameters.aveLenFlower.content != null && !visualParameters.aveLenFlower.content.isEmpty()) &&

                (growingTips.lighting != null && !growingTips.lighting.isEmpty()) &&
                (growingTips.watering.content != null && !growingTips.watering.content.isEmpty()) &&
                (growingTips.watering.measure != null && !growingTips.watering.measure.isEmpty()) &&
                (growingTips.tempreture.content != null && !growingTips.tempreture.content.isEmpty()) &&
                (growingTips.tempreture.measure != null && !growingTips.tempreture.measure.isEmpty());

    }

    public class VisualParameters{
        public String stemColour;
        public String leafColour;
        public AveLenFlower aveLenFlower;

        public VisualParameters() {
            aveLenFlower = new AveLenFlower();
        }

        public class AveLenFlower{
            public String content;
            public String measure;

            @Override
            public String toString() {
                return "AveLenFlower{" +
                        "content='" + content + '\'' +
                        ", measure='" + measure + '\'' +
                        '}';
            }

        }

        @Override
        public String toString() {
            return "VisualParameters{" +
                    "stemColour='" + stemColour + '\'' +
                    ", leafColour='" + leafColour + '\'' +
                    ", aveLenFlower=" + aveLenFlower +
                    '}';
        }
    }

    public class GrowingTips{
        public Tempreture tempreture;
        public Watering watering;
        public String lighting;

        public GrowingTips(){
            tempreture = new Tempreture();
            watering = new Watering();
        }

        public class Tempreture{
            public String content;
            public String measure;

            @Override
            public String toString() {
                return "Tempreture{" +
                        "content='" + content + '\'' +
                        ", measure='" + measure + '\'' +
                        '}';
            }
        }

        public class Watering{
            public String content;
            public String measure;

            @Override
            public String toString() {
                return "Watering{" +
                        "content='" + content + '\'' +
                        ", measure='" + measure + '\'' +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "GrowingTips{" +
                    "tempreture=" + tempreture +
                    ", watering=" + watering +
                    ", lighting='" + lighting + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", multiplying='" + multiplying + '\'' +
                ", visualParameters=" + visualParameters +
                ", growingTips=" + growingTips +
                '}';
    }
}
